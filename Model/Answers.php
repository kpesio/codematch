<?php
namespace Survey\SurveyPage\Model;

class Answers extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'survey_answer';
    //protected $_cacheTag = 'survey_answer';
    //protected $_eventPrefix = 'survey_answer';
    
    const ANSWER_ID = 'answer_id';
    const BEST_PRODUCT = 'bestproduct';
    const ANSWER_NAME = 'custname';
    const ANSWER_COMMENT = 'cmt';
    const ANSWER_RECOMMEND = 'recommend';        

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Survey\SurveyPage\Model\ResourceModel\Answers');
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    public function getName()
    {
        return $this->getData(self::ANSWER_NAME);
    }
    
    public function getComment()
    {
        return $this->getData(self::ANSWER_COMMENT);
    }
    
    public function getBestProduct()
    {
        return $this->getData(self::BEST_PRODUCT);
    }
    
    public function getRecommend()
    {
        return $this->getData(self::ANSWER_RECOMMEND);
    }
}