<?php
namespace Survey\SurveyPage\Model;

class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'survey_answer';
	protected $_cacheTag = 'survey_answer';
	protected $_eventPrefix = 'survey_answer';

	protected function _construct()
	{
		$this->_init('Survey\SurveyPage\Model\ResourceModel\Post');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];
		return $values;
	}
}
