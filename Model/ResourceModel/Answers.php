<?php
namespace Survey\SurveyPage\Model\ResourceModel;

class Answers extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    
    public function __construct(
	\Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }
    
    protected function _construct()
    {
        $this->_init('survey_answer', 'answer_id');
    }
}