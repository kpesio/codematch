<?php
namespace Survey\SurveyPage\Model\ResourceModel\Collection;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Survey\SurveyPage\Model\Answers', 'Survey\SurveyPage\Model\ResourceModel\Collection');
    }
}