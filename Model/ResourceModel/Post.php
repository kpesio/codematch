<?php
namespace Survey\SurveyPage\Model\ResourceModel;

class Post extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		// Define table & primary key
		$this->_init('survey_answer', 'answer_id');
	}
	
}
