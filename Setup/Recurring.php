<?php

namespace Survey\SurveyPage\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class Recurring implements InstallSchemaInterface
{
    const ANSWER_TABLE = 'survey_answer';
    const ANSWER_ID = 'answer_id';
    const ANSWER_BEST_PRODUCT = 'bestproduct';
    const ANSWER_NAME = 'custname';
    const ANSWER_COMMENT = 'cmt';
    const ANSWER_RECOMMEND = 'recommend';

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $coreResource;

    /**
     * @param \Magento\Framework\App\ResourceConnection $coreResource
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $coreResource
    ) {
        $this->coreResource = $coreResource;
    }

    /**
     * Stub for recurring setup script.
     *
     * For quick development and prototyping purposes we re-create our tables during every call to setup:upgrade,
     * regardless of version upgrades, which you normally would not do in production.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $setup
     * @throws
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $connection = $this->coreResource->getConnection();

        // Drop existing tables for quick prototyping. Comment out if you don't want this!
        if ($connection->isTableExists(self::ANSWER_TABLE)) {
            $connection->dropTable(self::ANSWER_TABLE);
        }

        $surveyTable = $connection->newTable(
            self::ANSWER_TABLE
        )->addColumn(
            self::ANSWER_ID,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Answer ID'
        )->addColumn(
           self::ANSWER_NAME,
           Table::TYPE_TEXT,
           null,
           ['nullable' => false],
           'Answer name'
        )->addColumn(
           self::ANSWER_COMMENT,
           Table::TYPE_TEXT,
           null,
           ['nullable' => false],
           'Comment'
        )->addColumn(
           self::ANSWER_BEST_PRODUCT,
           Table::TYPE_TEXT,
           null,
           ['nullable' => false],
           'Best product'
        )->addColumn(
           self::ANSWER_RECOMMEND,
           Table::TYPE_INTEGER,
           null,
           ['nullable' => false],
           'Recommend'
        );          
        
        $connection->createTable($surveyTable);

        $setup->endSetup();
    }
}